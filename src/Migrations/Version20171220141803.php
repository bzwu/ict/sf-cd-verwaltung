<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171220141803 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cd_song (cd_id INT NOT NULL, song_id INT NOT NULL, INDEX IDX_1CBDADED35F486F6 (cd_id), INDEX IDX_1CBDADEDA0BDB2F3 (song_id), PRIMARY KEY(cd_id, song_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cd_song ADD CONSTRAINT FK_1CBDADED35F486F6 FOREIGN KEY (cd_id) REFERENCES cd (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE cd_song ADD CONSTRAINT FK_1CBDADEDA0BDB2F3 FOREIGN KEY (song_id) REFERENCES song (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE song ADD name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE friend ADD firstname VARCHAR(255) NOT NULL, ADD lastname VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE loan ADD cd_id INT DEFAULT NULL, ADD friend_id INT DEFAULT NULL, ADD `from` DATETIME NOT NULL, ADD until DATETIME NOT NULL');
        $this->addSql('ALTER TABLE loan ADD CONSTRAINT FK_C5D30D0335F486F6 FOREIGN KEY (cd_id) REFERENCES cd (id)');
        $this->addSql('ALTER TABLE loan ADD CONSTRAINT FK_C5D30D036A5458E8 FOREIGN KEY (friend_id) REFERENCES friend (id)');
        $this->addSql('CREATE INDEX IDX_C5D30D0335F486F6 ON loan (cd_id)');
        $this->addSql('CREATE INDEX IDX_C5D30D036A5458E8 ON loan (friend_id)');
        $this->addSql('ALTER TABLE cd ADD name VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE cd_song');
        $this->addSql('ALTER TABLE cd DROP name');
        $this->addSql('ALTER TABLE friend DROP firstname, DROP lastname');
        $this->addSql('ALTER TABLE loan DROP FOREIGN KEY FK_C5D30D0335F486F6');
        $this->addSql('ALTER TABLE loan DROP FOREIGN KEY FK_C5D30D036A5458E8');
        $this->addSql('DROP INDEX IDX_C5D30D0335F486F6 ON loan');
        $this->addSql('DROP INDEX IDX_C5D30D036A5458E8 ON loan');
        $this->addSql('ALTER TABLE loan DROP cd_id, DROP friend_id, DROP `from`, DROP until');
        $this->addSql('ALTER TABLE song DROP name');
    }
}
