<?php
declare(strict_types=1);

namespace App\Form;

use App\Entity\Cd;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CdType
 *
 * @author  Jan Pache <jpache@dachcom.ch>
 * @package App\Form
 */
class CdType extends AbstractType
{
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'name' )
            ->add( 'submit', SubmitType::class );
    }
    
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( [
            'data_class' => Cd::class
        ] );
    }
    
    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'app_cd_form_type';
    }
    
}