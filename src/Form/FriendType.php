<?php
declare(strict_types=1);

namespace App\Form;

use App\Entity\Friend;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FriendType
 *
 * @author  Jan Pache <jpache@dachcom.ch>
 * @package App\Form
 */
class FriendType extends AbstractType
{
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'firstname' )
            ->add( 'lastname' )
            ->add( 'submit', SubmitType::class );
    }
    
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( [
            'data_class' => Friend::class
        ] );
    }
    
    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'app_song_form_type';
    }
    
}