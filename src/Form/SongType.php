<?php
declare(strict_types=1);

namespace App\Form;

use App\Entity\Cd;
use App\Entity\Song;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SongType
 *
 * @author  Jan Pache <jpache@dachcom.ch>
 * @package App\Form
 */
class SongType extends AbstractType
{
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'name' )
            ->add( 'cds', EntityType::class, [
                'class' => Cd::class,
                'choice_label' => 'name'
            ])
            ->add( 'submit', SubmitType::class );
    }
    
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( [
            'data_class' => Song::class
        ] );
    }
    
    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'app_song_form_type';
    }
    
}