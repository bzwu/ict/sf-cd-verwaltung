<?php
declare(strict_types=1);

namespace App\Form;

use App\Entity\Cd;
use App\Entity\Friend;
use App\Entity\Loan;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LoanType
 *
 * @author  Jan Pache <jpache@dachcom.ch>
 * @package App\Form
 */
class LoanType extends AbstractType
{
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add( 'cd', EntityType::class, [
                'class' => Cd::class,
                'choice_label' => 'name'
            ] )
            ->add( 'from', DateType::class, [
                'widget' => 'single_text'
            ] )
            ->add( 'until', DateType::class, [
                'widget' => 'single_text'
            ] )
            ->add( 'friend', EntityType::class, [
                'class' => Friend::class,
                'choice_label' => 'firstname'
            ] )
            ->add( 'submit', SubmitType::class );
    }
    
    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults( [
            'data_class' => Loan::class
        ] );
    }
    
    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'app_loan_form_type';
    }
}