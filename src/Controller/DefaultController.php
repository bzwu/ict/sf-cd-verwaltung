<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Cd;
use App\Entity\Friend;
use App\Entity\Loan;
use App\Entity\Song;
use App\Form\CdType;
use App\Form\FriendType;
use App\Form\LoanType;
use App\Form\SongType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 *
 * @author  Jan Pache <jpache@dachcom.ch>
 * @package App\Controller
 */
class DefaultController extends Controller
{
    private function insertForm( FormInterface $form, Request $request )
    {
        $form->handleRequest( $request );
        if ( $form->isSubmitted() && $form->isValid() ) {
            $em = $this->getDoctrine()->getManager();
            $entity = $form->getData();
            $em->persist( $entity );
            $em->flush();
        }
    }
    
    /**
     * @Route("/", name="homepage")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction( Request $request )
    {
        $loanForm = $this->createForm( LoanType::class );
        $this->insertForm( $loanForm, $request );
        
        return $this->render( 'app/index.html.twig', [
            'objectToAdd' => 'Loan',
            'form' => $loanForm->createView(),
            'currentData' => null
        ] );
    }
    
    /**
     * @Route("/cd/add/", name="cd_add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addCdAction( Request $request )
    {
        $cdForm = $this->createForm( CdType::class );
        $this->insertForm( $cdForm, $request );
        $repo = $this->getDoctrine()->getRepository( 'App:Cd' );
        $cds = $repo->findAll();
        $current = [];
    
        /** @var Cd $cd*/
        foreach ( $cds as $cd ) {
            $current[] = $cd->getName();
        }
        
        return $this->render( 'app/addObject.html.twig', [
            'objectToAdd' => 'CD',
            'form' => $cdForm->createView(),
            'currentData' => $current
        ] );
    }
    
    /**
     * @Route("/cd/list/", name="cd_list")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listCdAction( Request $request )
    {
        $em = $this->getDoctrine()->getManager();
        $cds = $em->getRepository( 'App:Cd' )->findAll();
        
        dump($cds[0]->getSongs());
        exit; // FIXME & remove debug
        
        return $this->render( 'app/listCd.html.twig', [
            'cds' => $cds
        ] );
    }
    
    /**
     * @Route("/song/add/", name="song_add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addSongAction( Request $request )
    {
        $songForm = $this->createForm( SongType::class );
        $this->insertForm( $songForm, $request );
        $repo = $this->getDoctrine()->getRepository( 'App:Song' );
        $songs = $repo->findAll();
        $current = [];
    
        /** @var Song $song*/
        foreach ( $songs as $song ) {
            $current[] = $song->getName();
        }
        
        return $this->render( 'app/addObject.html.twig', [
            'objectToAdd' => 'Song',
            'form' => $songForm->createView(),
            'currentData' => $current
        ] );
    }
    
    /**
     * @Route("/friend/add/", name="friend_add")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addFriendAction( Request $request )
    {
        $friendForm = $this->createForm( FriendType::class );
        $this->insertForm( $friendForm, $request );
        $repo = $this->getDoctrine()->getRepository( 'App:Friend' );
        $friends = $repo->findAll();
        $current = [];
    
        /** @var Friend $friend*/
        foreach ( $friends as $friend ) {
            $current[] = $friend->__toString();
        }
        
        return $this->render( 'app/addObject.html.twig', [
            'objectToAdd' => 'Friend',
            'form' => $friendForm->createView(),
            'currentData' => $current
        ] );
    }
}