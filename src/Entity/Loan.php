<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Loan
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\LoanRepository")
 * @ORM\Table(name="loan")
 */
class Loan
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $from;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $until;
    
    /**
     * @var Cd
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Cd", inversedBy="loans")
     */
    private $cd;
    
    /**
     * @var Friend
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Friend", inversedBy="loans")
     */
    private $friend;
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @return \DateTime
     */
    public function getFrom(): \DateTime
    {
        return $this->from;
    }
    
    /**
     * @param \DateTime $from
     *
     * @return Loan
     */
    public function setFrom( \DateTime $from ): Loan
    {
        $this->from = $from;
        
        return $this;
    }
    
    /**
     * @return \DateTime
     */
    public function getUntil(): \DateTime
    {
        return $this->until;
    }
    
    /**
     * @param \DateTime $until
     *
     * @return Loan
     */
    public function setUntil( \DateTime $until ): Loan
    {
        $this->until = $until;
        
        return $this;
    }
    
    /**
     * @return Cd
     */
    public function getCd(): Cd
    {
        return $this->cd;
    }
    
    /**
     * @param Cd $cd
     *
     * @return Loan
     */
    public function setCd( Cd $cd ): Loan
    {
        $this->cd = $cd;
        
        return $this;
    }
    
    /**
     * @return Friend
     */
    public function getFriend(): Friend
    {
        return $this->friend;
    }
    
    /**
     * @param Friend $friend
     *
     * @return Loan
     */
    public function setFriend( Friend $friend ): Loan
    {
        $this->friend = $friend;
        
        return $this;
    }
}
