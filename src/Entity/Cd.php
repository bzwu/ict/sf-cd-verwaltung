<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Cd
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\CdRepository")
 * @ORM\Table(name="cd")
 */
class Cd
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;
    
    /**
     * @var Song[]|Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Song", inversedBy="cds")
     */
    private $songs;
    
    /**
     * @var Loan[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Loan", mappedBy="cd")
     */
    private $loans;
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    
    /**
     * @param string $name
     *
     * @return Cd
     */
    public function setName( string $name ): Cd
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * @return Song[]|ArrayCollection
     */
    public function getSongs()
    {
        return $this->songs;
    }
    
    /**
     * @param Song[]|ArrayCollection $songs
     *
     * @return Cd
     */
    public function setSongs( $songs ): Cd
    {
        $this->songs = $songs;
        
        return $this;
    }
    
    /**
     * @return Loan[]|ArrayCollection
     */
    public function getLoans()
    {
        return $this->loans;
    }
    
    /**
     * @param Loan[]|ArrayCollection $loans
     *
     * @return Cd
     */
    public function setLoans( $loans ): Cd
    {
        $this->loans = $loans;
        
        return $this;
    }
}
