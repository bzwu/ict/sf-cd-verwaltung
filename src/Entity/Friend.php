<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Friend
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\FriendRepository")
 * @ORM\Table(name="friend")
 */
class Friend
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $firstname;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $lastname;
    
    /**
     * @var Loan[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Loan", mappedBy="friend")
     */
    private $loans;
    
    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf( '%s %s', $this->getFirstname(), $this->getLastname() );
    }
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }
    
    /**
     * @param string $firstname
     *
     * @return Friend
     */
    public function setFirstname( string $firstname ): Friend
    {
        $this->firstname = $firstname;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }
    
    /**
     * @param string $lastname
     *
     * @return Friend
     */
    public function setLastname( string $lastname ): Friend
    {
        $this->lastname = $lastname;
        
        return $this;
    }
    
    /**
     * @return Loan[]|ArrayCollection
     */
    public function getLoans()
    {
        return $this->loans;
    }
    
    /**
     * @param Loan[]|ArrayCollection $loans
     *
     * @return Friend
     */
    public function setLoans( $loans ): Friend
    {
        $this->loans = $loans;
        
        return $this;
    }
}
