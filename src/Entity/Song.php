<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Song
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\SongRepository")
 * @ORM\Table(name="song")
 */
class Song
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;
    
    /**
     * @var Cd[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Cd", mappedBy="songs")
     */
    private $cds;
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    
    /**
     * @param string $name
     *
     * @return Song
     */
    public function setName( string $name ): Song
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * @return Cd[]|ArrayCollection
     */
    public function getCds()
    {
        return $this->cds;
    }
    
    /**
     * @param Cd[]|ArrayCollection $cds
     *
     * @return Song
     */
    public function setCds( $cds ): Song
    {
        $this->cds = $cds;
        
        return $this;
    }
}
